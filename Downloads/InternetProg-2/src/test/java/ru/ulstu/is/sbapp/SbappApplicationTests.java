package ru.ulstu.is.sbapp;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.ulstu.is.sbapp.speaker.service.SpeakerService;

@SpringBootTest
class SbappApplicationTests {
	
	private final Logger log = LoggerFactory.getLogger(SbappApplicationTests.class);
	
    @Autowired
    SpeakerService speakerService;

	@Test
	void testCountCon() {
		final String countCon = speakerService.say("User", "con");
		log.info("Stroka: " + countCon);
		log.info("Primer: Count consonants: 2, User!");
		Assertions.assertEquals("Count consonants: 2, User!", countCon);
	}

	@Test
	void testCountVow() {
		final String countVow = speakerService.say("Maria", "vow");
		log.info("Stroka: " + countVow);
		log.info("Primer: Count vowels: 3, Maria!");
		Assertions.assertEquals("Count vowels: 3, Maria!", countVow);
	}
}
