package ru.ulstu.is.sbapp.speaker.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SpeakerVow implements Speaker {
    private final Logger log = LoggerFactory.getLogger(SpeakerVow.class);

    @Override
    public String count(String name){
    	char[] vo = new char[]{ 
    			'a', 'e', 'i', 'o', 'u',
    			'A', 'E', 'I', 'O', 'U'
    			};
    	
    	int counter_vo = 0;
    	
        char[] namearr = name.toCharArray();
        
        for (int i = 0; i < namearr.length; i++) {
            for (int a = 0; a < vo.length; a++) {
                if (namearr[i] == vo[a]){
                    counter_vo++;
                    break;
                }
            }
        }
        return "Count vowels: " + Integer. toString(counter_vo);
    }

    public void init(){
        log.info("SpeakerVow.init()");
    }

    public void destroy(){
        log.info("SpeakerVow.destroy()");
    }
}

