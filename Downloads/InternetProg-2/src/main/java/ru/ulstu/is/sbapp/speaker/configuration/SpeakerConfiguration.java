package ru.ulstu.is.sbapp.speaker.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.ulstu.is.sbapp.speaker.domain.Speaker;
import ru.ulstu.is.sbapp.speaker.domain.SpeakerCon;
import ru.ulstu.is.sbapp.speaker.domain.SpeakerVow;

@Configuration
public class SpeakerConfiguration {
    private final Logger log = LoggerFactory.getLogger(SpeakerConfiguration.class);

    @Bean(value = "con", initMethod = "init", destroyMethod = "destroy")
    public Speaker createCon() {
        log.info("Call createCon()");
        return new SpeakerCon();
    }

    @Bean(value = "vow", initMethod = "init", destroyMethod = "destroy")
    public Speaker createVow() {
        log.info("Call createVow()");
        return new SpeakerVow();
    }
}
