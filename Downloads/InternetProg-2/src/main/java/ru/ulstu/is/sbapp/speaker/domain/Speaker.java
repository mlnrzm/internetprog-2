package ru.ulstu.is.sbapp.speaker.domain;

public interface Speaker {
    String count(String name);
}
