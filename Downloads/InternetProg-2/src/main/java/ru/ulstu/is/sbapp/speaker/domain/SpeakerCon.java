package ru.ulstu.is.sbapp.speaker.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SpeakerCon implements Speaker {
    private final Logger log = LoggerFactory.getLogger(SpeakerCon.class);

    @Override
    public String count(String name){
    	char[] con = new char[] { 
    			'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z',
    			'B', 'C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'X', 'Y', 'Z'
    			};
    	int counter_con = 0;
    	
        char[] namearr = name.toCharArray();
        
        for (int i = 0; i < namearr.length; i++) {

            for (int a = 0; a < con.length; a++) {
                if (namearr[i] == con[a]){
                    counter_con++;
                    break;
                }
            }
        }
        return "Count consonants: " + Integer. toString(counter_con);
    }

    public void init(){
        log.info("SpeakerCon.init()");
    }

    public void destroy(){
        log.info("SpeakerCon.destroy()");
    }
}
