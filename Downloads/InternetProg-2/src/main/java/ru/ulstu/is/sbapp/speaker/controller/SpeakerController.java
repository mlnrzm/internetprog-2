package ru.ulstu.is.sbapp.speaker.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.ulstu.is.sbapp.speaker.service.SpeakerService;

@RestController
public class SpeakerController {
    private final SpeakerService speakerService;

    public SpeakerController(SpeakerService speakerService) {
        this.speakerService = speakerService;
    }

    @GetMapping("/")
    public String count(@RequestParam(value = "name", defaultValue = "User") String name,
                        @RequestParam(value = "letters", defaultValue = "con") String letters) {
        return speakerService.say(name, letters);
    }
}
